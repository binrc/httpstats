The most interesting component is a shell script that generates a CSV from standard `OpenBSD httpd` log files. 

~~This shell script is then fed to R and processed with Rmarkdown to create a static web page~~

This shell script generates a CSV, modifies that same CSV because I'm bad at awk, then grabs some data from it. This data is then dumped into some sort of HTML file. 

This script runs as a cronjob, I have included a sample crontab for the noobs among us.

I have excluded log files from my repository. They contain IP addresses. I log IP addresses so that I have some way of distinguishing state sponsored hackers from scrapers who haven't figured out that I publish everything I do on gitlab. 
