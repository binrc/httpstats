#!/bin/sh
headstub=$(cat headstub.html);
footstub=$(cat footstub.html);

mkdir /tmp/httplogs
cd /tmp/httplogs
cp -rv /var/www/logs/access.* ./

# unzip
for i in $(ls *.gz); do
	gunzip $i;
done;

echo > main.log;

for i in $(ls *.log* | grep -v  main.log); do
	cat $i >> main.log;
done;

#echo '"VHOST","IP","DD","MM","YY","hh","mm","ss","REQUEST","STATUS"' > main.csv
#cat main.log | awk -F' '  'BEGIN{FPAT="([^ ]+)|(\"[^\"]+\")"}{!/^$/}{gsub("/","\",\"", $5)}{gsub(":", "\",\"", $5)}{print "\"" $1 "\",\"" $2 "\",\"" $5$6 "\"," $7 ",\"" $8 "\""}' | grep -v "turnedover" | grep -v '"","","",,""' | sed -e 's/\[//g' -e 's/\+0000]//g' >> main.csv

echo '"VHOST","IP","DATE","REQMETH","FILE","HTTPV","STATUS"' > main.csv
cat main.log | awk -F' '  'BEGIN{FPAT="([^ ]+)|(\"[^\"]+\")"}{!/^$/}{print $1 "," $2 "," $5$6 "," $7 "," $8 "," $9 "," $10 }' | grep -v "turnedover" | grep -v ',,,,,,' | sed -e 's/\[//g' -e 's/\:..\:..\:..\+0000]//g' -e 's/\"//g' >> main.csv

cat main.csv | sed '1d' | awk -F',' '\
	{gsub("Jan", "01")}\
	{gsub("Feb", "02")}\
	{gsub("Mar", "03")}\
	{gsub("Apr", "04")}\
	{gsub("May", "05")}\
	{gsub("Jun", "06")}\
	{gsub("Jul", "07")}\
	{gsub("Aug", "08")}\
	{gsub("Sep", "09")}\
	{gsub("Oct", "10")}\
	{gsub("Nov", "11")}\
	{gsub("Dec", "12")}\
		{split($3, a, "/")}\
	{print $1 "," $2 "," a[3] "-" a[2] "-" a[1] "," $4 "," $5 "," $6 "," $7}' > betterDates

mv betterDates main.csv

ents=$(cat main.csv | cut -d ',' -f 3 | sort | uniq );

rm dates.csv

for i in $ents; do
	        oc=$(grep $i main.csv | wc -l | tr -d '[:space:]' );
		        printf "$i\t$oc\n" >> dates.csv
done;


ts=$(date)

sd=$(cat main.csv | cut -d',' -f 3 | sort | head -n 1);
ed=$(cat main.csv | cut -d',' -f 3 | sort | tail -n 1);
total=$(cat main.csv | wc -l)
uhosts=$(cat main.csv | cut -d',' -f 2 | sort | uniq | wc -l)
haxattempts=$(cat /tmp/httplogs/main.csv | grep -v GET | grep -v HEAD | wc -l);
rss=$(cat main.csv | grep "feed.php" | wc -l)
urss=$(cat main.csv | grep "feed.php" | awk -F',' '{print $2$5}' | sort | uniq | wc -l)
onions=$(cat main.csv | grep "ilsstfnqt4vpykd2bqc7ntxf2tqupqzi6d5zmk767qtingw2vp2hawyd.onion" | wc -l)
eepsites=$(cat main.csv | grep "xzh77mcyknkkghfqpwgzosukbshxq3nwwe2cg3dtla7oqoaqknia.b32.i2p" | wc -l)





# generate plot
echo "set term png; set xdata time; set timefmt '%Y-%m-%d'; set datafile separator '\t'; set xlabel 'DATE'; set ylabel 'REQUESTS'; plot 'dates.csv' u 1:2 smooth csplines title '0x19 traffic';"  | /usr/local/bin/gnuplot > traffic.png


echo $headstub > index.html
echo "<h1>0x19.org Statistics</h1>" >> index.html
echo "<p> Last generated on: $ts </p>" >> index.html
echo "<p> Date range: $sd through $ed</p>" >> index.html
echo "<p> Total requests: $total</p>" >> index.html
echo "<p> Unique clients: $uhosts</p>" >> index.html
echo "<p> Number of attempted hacks (ie requests other than GET or HEAD): $haxattempts</p>" >> index.html
echo "<p> Total RSS pings: $rss</p>" >> index.html
echo "<p> Unique RSS clients (by IP): $urss</p>" >> index.html
echo "<p> Total requests over Tor: $onions</p>" >> index.html
echo "<p> Total requests over i2p: $eepsites</p>" >> index.html
echo "<p><img src='traffic.png' alt='traffic graph by date'></p>" >> index.html
echo $footstub >> index.html

cp ./index.html /var/www/htdocs/httpstats/index.html
cp ./traffic.png /var/www/htdocs/httpstats/traffic.png

sleep 10;

cd -

rm -rf /tmp/httplogs
